#include <FastLED.h>

/* Get serial messages (38400) to control 3 strips of leds WS2812B/NEOPIXEL
 *  
 * Wiring Nano : GND, D2, D3, D4
 * 
 * Messages <mode> <led/hue> <brightness/fps> :
 * - 0, 1, 2: <strip> <led> <brightness> (set led brightness)
 * - 3, 4, 5: <strip> <hue> <brightness> (set strip led hue & brightness)
 * - 6: <nb_leds> <brightness> (set all brightness)
 * - 7: <hue> <brightness> (set all hue & brightness)
 * 
 * FastLED doc : http://fastled.io/docs/3.1/index.html
 * LED specs: color bits: 8 | data rate: 800kbps | pwm rate: 400Hz | Chipset Power Draw : 5mw/1ma@5v
 */
      
// Setup 3 strips of 60 Leds
#define NUM_STRIPS  3
#define NUM_LEDS    60 // 1m
#define LED_TYPE    WS2812B //NEOPIXEL

CRGB leds[NUM_STRIPS][NUM_LEDS];

// Incoming data from serial messages
int mode = 0;
int v1 = 0;
int v2 = 0;
int hues[] = { 50, 100, 200 }; // default hue values

void setup() {
  delay(2000); // delay for recovery

  // Setup leds array
  FastLED.addLeds<LED_TYPE, 2>(leds[0], NUM_LEDS);
  FastLED.addLeds<LED_TYPE, 3>(leds[1], NUM_LEDS);
  FastLED.addLeds<LED_TYPE, 4>(leds[2], NUM_LEDS);

  // Setup color black (no color) and brightness
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[0][i] = CRGB::Black;
    leds[1][i] = CRGB::Black;
    leds[2][i] = CRGB::Black;
   }
  FastLED.setBrightness(255);
  FastLED.show();

  // Serial communication
  Serial.begin(9600);
}

void loop(){ 

}

void serialEvent() {
  // Get serial messages
  while (Serial.available() > 0) {

     // data
     mode = Serial.parseInt();
     v1 = Serial.parseInt();
     v2 = Serial.parseInt();

     // do
     if (Serial.read() == '\n') {
      
      if (mode <= 2) {
        // mode = strip, v1 = led, v2 = brightness
        leds[mode][v1] = CHSV( hues[mode], 255, v2 );
      }
      else if (mode >= 3 && mode <= 5) {
        // mode = strip, v1 = led, v2 = brightness
        for (int i = 0; i < NUM_LEDS; i++) {
            hues[mode - 3] = v1;
            leds[mode - 3][i] = CHSV( hues[mode - 3], 255, v2 );
          }
      } else if (mode == 6) {
        // v1 = nb leds, v2 = brightness
        for (int i = 0; i < v1; i++) {
           leds[0][i] = CHSV( hues[0], 255, v2 );
           leds[1][i] = CHSV( hues[1], 255, v2 );
           leds[2][i] = CHSV( hues[2], 255, v2 );
          }
      } else if (mode == 7) {
        //v1 = hue, v2 = brightness
        for (int i = 0; i < NUM_LEDS; i++) {
           leds[0][i] = CHSV( v1, 255, v2 );
           leds[1][i] = CHSV( v1, 255, v2 );
           leds[2][i] = CHSV( v1, 255, v2 );
          }
      }
      
      FastLED.show();
      
     }//serial.read
     
  }//serial.available
}
