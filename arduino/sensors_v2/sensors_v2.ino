
/* 
 * SENSORS TO SERIAL
 * 
 * Licence: GNU/GPL3
 * Date: 2021.09
 * Website: http://framagit.org/totem/totem-tech
 * Author: Jérôme Abel
 *
 * Libraries:
 * - RotaryEncoder from Matthias Hertel, http://www.mathertel.de
 * - ezButton from ArduinoGetStarted, https://arduinogetstarted.com/tutorials/arduino-button-library
 * 
 * Pins:
 * - 6 ana (5 pot, 1 distance) : A0, A1, A2, A3, A4, A5 (distance)
 * - 1 encoder : D2 (CLK), D3 (DT)
 * - 6 digit (btn) : D4, D5, D6, D7, D8, D9
 * 
 * Input serial command:
 * Activate sensors : <analog sensor id>(0-6) <state>(0-1)
 */

#include <RotaryEncoder.h>
#include <ezButton.h>

#define ENC_PIN1 2 // pin encoder CLK
#define ENC_PIN2 3 // pin encoder DT

#define ANA_NB 6 // number of analog inputs
#define ANA_STATE 1 // or 0 to disable analog sensors at startup
#define ANA_HYSTERESIS 2 // avoid noise
#define ANA_SAMPLING_RATE 50 // ms

#define DIGIT_NB 6 // number of digital inputs
#define DIGIT_DEBOUNCE_TIME 50 // avoid noise push buttons (in ms)


// Analog values
int anaCurrentValues[ANA_NB];
int anaPreviousValues[ANA_NB];
int anaState[ANA_NB];
unsigned long now = 0; // variable for sampling rate

// Array of buttons
ezButton b1(4);
ezButton b2(5);
ezButton b3(6);
ezButton b4(7);
ezButton b5(8);
ezButton b6(9);
ezButton buttons[] = { b1, b2, b3, b4, b5, b6 };

// Encoder
RotaryEncoder encoder(ENC_PIN1, ENC_PIN2, RotaryEncoder::LatchMode::TWO03);

void setup()
{
  Serial.begin(38400);
  //while (! Serial);

  // Encoder setup
  attachInterrupt(digitalPinToInterrupt(ENC_PIN1), checkPosition, CHANGE);
  attachInterrupt(digitalPinToInterrupt(ENC_PIN2), checkPosition, CHANGE);

  // Set all analog sensors to on if you don't want to switch on with incoming serial messages
  setAllAnaState(ANA_STATE);
  
  // Buttons debounce time
  for (int i = 0; i < DIGIT_NB; i++) {
    buttons[i].setDebounceTime(DIGIT_DEBOUNCE_TIME);
  }
}

void loop()
{
  sendAna();
  sendDigit();
  sendEncoder();
}

void setAllAnaState(int on) {
  for (int i = 0; i < ANA_NB; i++) {
    anaState[i] = on;
  }
}

void sendAna(){

    // sampling rate
    while(millis() > now + ANA_SAMPLING_RATE){

      // check every analog sensors
      for (int i = 0; i < ANA_NB; i++) {

        // if analog sensor is set to on
        if (anaState[i] > 0) {
          anaCurrentValues[i] = (int) analogRead(i);
          // hysteresis
          if (  (anaCurrentValues[i] >= (anaPreviousValues[i] + ANA_HYSTERESIS)) || (anaCurrentValues[i] <= (anaPreviousValues[i] - ANA_HYSTERESIS)) ) {
            sendSerial("a ", i, anaCurrentValues[i]);
          }
          anaPreviousValues[i] = anaCurrentValues[i];
        }
        
      } // for

      now = millis();
    } // while
}

void sendDigit(){  
  for (int i = 0; i < DIGIT_NB; i++) {
    buttons[i].loop();
    if(buttons[i].isPressed()) sendSerial("d ", i, 1);
    else if(buttons[i].isReleased()) sendSerial("d ", i, 0);
  }
}

void sendEncoder(){
  static int pos = 0;
  encoder.tick();
  int newPos = encoder.getPosition();
  if (pos != newPos) {
    // newPos
    int dir = (int)(encoder.getDirection());
    Serial.print("e ");
    Serial.println(dir);
    pos = newPos;
  }
}

void checkPosition() 
{ 
  encoder.tick(); 
}

void sendSerial(String key, int id, int val) {
  Serial.print(key);
  Serial.print(id);
  Serial.print(" ");
  Serial.println(val);
}

// Input serial messages (analog state)
void serialEvent() {
  // Get serial messages
  while (Serial.available() > 0) {

     // data
     int sensorId = Serial.parseInt();
     int on = Serial.parseInt();

     // do
     if (Serial.read() == '\n') {
      anaState[sensorId] = on;
     }

  } // while
}
