#include <FastLED.h>

/* 
 * Get serial messages (38400) to control 3 strips of leds WS2812B/NEOPIXEL
 *  
 * Wiring Nano : GND, D2, D3, D4
 * 
 * Messages <mode> <led/hue> <brightness/fps> :
 * - 0, 1, 2: <strip> <led> <brightness> (set led brightness)
 * - 3, 4, 5: <strip> <hue> <brightness> (set strip led hue & brightness)
 * - 6: <nb_leds> <brightness> (set all brightness)
 * - 7: <hue> <brightness> (set all hue & brightness)
 * - 8: <hue> <fps> (trigger pattern solo)
 * - 9: <hue> <fps>(trigger pattern rainbow)
 *  
 * FastLED doc : http://fastled.io/docs/3.1/index.html
 * LED specs: color bits: 8 | data rate: 800kbps | pwm rate: 400Hz | Chipset Power Draw : 5mw/1ma@5v
 *
 */
      
// Setup 3 strips of 60 Leds
#define NUM_STRIPS  3
#define NUM_LEDS    60 // 1m
#define LED_TYPE    WS2812B //NEOPIXEL
#define COLOR_ORDER GRB // RGB
#define BAUD_RATE   9600

CRGB leds[NUM_STRIPS][NUM_LEDS];

// Incoming data from serial messages
int mode = 0;
int v1 = 0;
int v2 = 0;
int hues[] = { 50, 100, 200 }; // default hue values

uint8_t currentPattern = 0; // Index number of which pattern is current
int currentBritghness = 255;
uint8_t currentHue = 0;
int currentFPS = 80;

void patternSolo()
{
  fadeToBlackBy( leds[0], NUM_LEDS, 20 );
  fadeToBlackBy( leds[1], NUM_LEDS, 20 );
  fadeToBlackBy( leds[2], NUM_LEDS, 20 );
  int pos = beatsin16( 13, 0, NUM_LEDS-1 );
  leds[0][pos] += CHSV( currentHue, 255, 192 );
  leds[1][pos] += CHSV( currentHue, 255, 192 );
  leds[2][pos] += CHSV( currentHue, 255, 192 );
}

void patternRainbow() 
{
  // FastLED's built-in rainbow generator
  fill_rainbow( leds[0], NUM_LEDS, currentHue, 7);
  fill_rainbow( leds[1], NUM_LEDS, currentHue, 7);
  fill_rainbow( leds[2], NUM_LEDS, currentHue, 7);
  currentHue++;
}


void setup() {
  delay(2000); // delay for recovery

  // Setup leds array
  FastLED.addLeds<LED_TYPE, 2, COLOR_ORDER>(leds[0], NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.addLeds<LED_TYPE, 3, COLOR_ORDER>(leds[1], NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.addLeds<LED_TYPE, 4, COLOR_ORDER>(leds[2], NUM_LEDS).setCorrection(TypicalLEDStrip);

  // Setup color black (no color) and brightness
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[0][i] = CRGB::Black;
    leds[1][i] = CRGB::Black;
    leds[2][i] = CRGB::Black;
   }
  FastLED.setBrightness(255);
  FastLED.show();

  // Serial communication
  Serial.begin(BAUD_RATE);
}

void loop(){ 
  // Patterns update
  if (currentPattern > 0) {
    if (currentPattern == 1) patternSolo();
    else if (currentPattern == 2) patternRainbow();
    FastLED.show();  
    FastLED.delay(1000 / currentFPS);
  }
}

void serialEvent() {
  // Get serial messages
  while (Serial.available() > 0) {

     // data
     mode = Serial.parseInt();
     v1 = Serial.parseInt();
     v2 = Serial.parseInt();

     // switch off pattern
     currentPattern = 0;

     // do
     if (Serial.read() == '\n') {
      
      if (mode <= 2) {
        // mode = strip, v1 = led, v2 = brightness
        leds[mode][v1] = CHSV( hues[mode], 255, v2 );
      }
      else if (mode >= 3 && mode <= 5) {
        // mode = strip, v1 = led, v2 = brightness
        for (int i = 0; i < NUM_LEDS; i++) {
            hues[mode - 3] = v1;
            leds[mode - 3][i] = CHSV( hues[mode - 3], 255, v2 );
          }
      } else if (mode == 6) {
        // v1 = nb leds, v2 = brightness
        for (int i = 0; i < v1; i++) {
           leds[0][i] = CHSV( hues[0], 255, v2 );
           leds[1][i] = CHSV( hues[1], 255, v2 );
           leds[2][i] = CHSV( hues[2], 255, v2 );
          }
      } else if (mode == 7) {
        //v1 = hue, v2 = brightness
        for (int i = 0; i < NUM_LEDS; i++) {
           leds[0][i] = CHSV( v1, 255, v2 );
           leds[1][i] = CHSV( v1, 255, v2 );
           leds[2][i] = CHSV( v1, 255, v2 );
          }
      } else if (mode == 8) {
        //v1 = hue, v2 = fps
        currentPattern = 1;
        currentHue = v1;
        currentFPS = v2;
      } else if (mode == 9) {
        //v1 = hue, v2 = fps
        currentPattern = 2;
        currentHue = v1;
        currentFPS = v2;
      }
      
      FastLED.show();
      
     }
     
  }//serial.available
}
