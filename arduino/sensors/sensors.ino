
/* 
 * SENSORS TO SERIAL (Mega)
 * Serial command input to activate sensors : <analog sensor id> (0-6) <state>(0-1)
 *
 * Licence : GNU/GPL3
 * Date : 2021.06
 * Website : http://framagit.org/totem/totem-tech
 *
 * RotaryEncoder lib from Matthias Hertel, http://www.mathertel.de
 * 
 * Pins:
 * - 7 ana (5 pot, 1 pressure, 1 distance) : A0, A1, A2, A3, A4, A5 (pressure), A6 (distance)
 * - 1 encoder : D2 (CLK), D3 (DT)
 * - 3 digit (btn) : D4, D5, D6
 */

#include <RotaryEncoder.h>
#include <ButtonDebounce.h>

#define ENC_PIN1 2
#define ENC_PIN2 3
#define ANA_NB 7
#define DIGIT_NB 3

// Sampling rate
#define SAMPLING_RATE 40
unsigned long now = 0;

// Current and previous values
const int hysteresis = 2;
int anaCurrentValues[ANA_NB];
int anaPreviousValues[ANA_NB];
int anaState[ANA_NB];
int digitCurrentValues[DIGIT_NB];
int digitPreviousValues[DIGIT_NB];

// Buttons array
ButtonDebounce b1(4, 100);
ButtonDebounce b2(5, 100);
ButtonDebounce b3(6, 100);
ButtonDebounce buttons[] = { b1, b2, b3 };

// Encoder
RotaryEncoder encoder(ENC_PIN1, ENC_PIN2, RotaryEncoder::LatchMode::TWO03);

void setup()
{
  Serial.begin(38400);
  while (! Serial);

  // Encoder setup
  attachInterrupt(digitalPinToInterrupt(ENC_PIN1), checkPosition, CHANGE);
  attachInterrupt(digitalPinToInterrupt(ENC_PIN2), checkPosition, CHANGE);

  /*
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);
  */

  // Set all analog sensors to on if you don't want to switch on with incoming serial messages
  // setAnaState(1);
}

void loop()
{
  sendAna();
  sendDigit();
  sendEncoder();
  delay(10);
}

void setAnaState(int on) {
  for (int i = 0; i < ANA_NB; i++) {
    anaState[i] = on;
  }
}

void sendAna(){

    // sampling rate
    while(millis() > now + SAMPLING_RATE){

      // check every analog sensors
      for (int i = 0; i < ANA_NB; i++) {

        // if analog sensor is set to on
        if (anaState[i] > 0) {
          anaCurrentValues[i] = (int) analogRead(i);
          // hysteresis
          if (  (anaCurrentValues[i] >= (anaPreviousValues[i] + hysteresis)) || (anaCurrentValues[i] <= (anaPreviousValues[i] - hysteresis)) ) {
            Serial.print("a ");
            Serial.print(i);
            Serial.print(" ");
            Serial.println(anaCurrentValues[i]);  
          }
          anaPreviousValues[i] = anaCurrentValues[i];
        }
        
      } // for

      now = millis();
    } // while
}

void sendDigit(){
  for (int i = 0; i < DIGIT_NB; i++) {
    buttons[i].update();
    digitCurrentValues[i] = buttons[i].state();
    if (digitCurrentValues[i] != digitPreviousValues[i]) {
      if(buttons[i].state() == LOW){
        sendSerial("d ", i);
      }
    }
    digitPreviousValues[i] = digitCurrentValues[i];
  }  
}

void sendEncoder(){
  static int pos = 0;
  encoder.tick();
  int newPos = encoder.getPosition();
  if (pos != newPos) {
    // newPos
    int dir = (int)(encoder.getDirection());
    sendSerial("e ", dir);
    pos = newPos;
  }
}

void checkPosition() 
{ 
  encoder.tick(); 
}

void sendSerial(String key, int val) {
  Serial.print(key);
  Serial.println(val);
}


void serialEvent() {
  // Get serial messages
  while (Serial.available() > 0) {

     // data
     int sensorId = Serial.parseInt();
     int on = Serial.parseInt();

     // do
     if (Serial.read() == '\n') {
      anaState[sensorId] = on;
     }

  } // while
}
