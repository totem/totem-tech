/* 
 *  DC motor (channel A) serial control
 *  command : <speed> (0-255) <dir> (0-1)
 */ 

const int speedPin = 3;
const int dirPin = 12;
int pwm = 0;
int dir = 0;
int lastDir = 0;

void setup(void)
{
  // Setup motor pins
  pinMode(speedPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  Serial.begin(9600);
}

void loop(void)
{

  // Get serial message
  while (Serial.available()) {
     pwm = Serial.parseInt();
     dir = Serial.parseInt();
     if (Serial.read() == '\n') {
      if (dir != lastDir) digitalWrite(dirPin, dir);
      analogWrite(speedPin, pwm);
     }
     lastDir = dir;
   }

}


/*
 * Leonardo/ romeo v2
 * int E1 = 5; // M1 Speed Control
 * int M1 = 4; // M1 Direction Control
 * 
 * Nano motor shield
 * int E1 = 3; // M1 Speed Control
 * int M1 = 12; // M1 Direction Control
 * Nano motor shield
 * CH A :  DIR(D12), PWM (D3), Frein (D9), Detection courant (A0) 
 * CH B :  DIR(D13), PWM (D11), Frein (D8), Detection courant (A1)
 * 
 * 
 * int SPEED_MIN = 35; // romeo(32. 4.4V  des fois 41) / nano (35)
 * int SPEED_MAX = 49; // romeo(46 = 6V quand alim 12V) / nano (49)
 */
 
// digitalWrite(E1,LOW); // stop
// analogWrite (E1,a);  // PWM Speed Control
// digitalWrite(M1,HIGH); // forward
// digitalWrite(M1,LOW); // backward
