
/* 
 * TEST BUTTONS (SERIAL)
 * Serial command input to activate sensors : <analog sensor id> (0-6) <state>(0-1)
 *
 * Licence: GNU/GPL3
 * Date: 2021.09
 * Website: http://framagit.org/totem/totem-tech
 * Author: Jérôme Abel
 *
 * Library:
 * - ezButton from ArduinoGetStarted, https://arduinogetstarted.com/tutorials/arduino-button-library
 * 
 * Pins:
 * - 6 digit (btn) : D4, D5, D6, D7, D8, D9
 */

#include <ezButton.h>

#define DIGIT_NB 6

// Buttons
ezButton b1(4);
ezButton b2(5);
ezButton b3(6);
ezButton b4(7);
ezButton b5(8);
ezButton b6(9);
ezButton buttons[] = { b1, b2, b3, b4, b5, b6};

void setup()
{
  Serial.begin(38400);

  // Buttons debounce time
  for (int i = 0; i < DIGIT_NB; i++) {
    buttons[i].setDebounceTime(50);
  }
}

void loop()
{
  sendDigit();
}

void sendDigit(){  
  for (int i = 0; i < DIGIT_NB; i++) {
    buttons[i].loop();
    if(buttons[i].isPressed()) sendSerial("d ", i, 1);
    else if(buttons[i].isReleased()) sendSerial("d ", i, 0);
  }
}

void sendSerial(String key, int id, int val) {
  Serial.print(key);
  Serial.print(id);
  Serial.print(" ");
  Serial.println(val);
}
