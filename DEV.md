# TOTEM - electro



ventilation.

## RPI POWER
RPI USV+ UPS+ for the Raspberry Pi  https://www.reichelt.com/de/en/ups-for-the-raspberry-pi-rpi-usv--p169883.html?&nbc=1&SID=93488d0e8200f3a04a320b29f5fcd988546c8fe60502846eafc2f&trstct=adw_161587
The USV+ is an extension board for the Raspberry Pi. With this extension, you can ensure that the Pi is safely shut down in the event of a power failure.
Corrupt and defective file systems on the SD card are a thing of the past.

## Motor


https://www.pololu.com/category/60/micro-metal-gearmotors

https://www.pololu.com/product/989

https://www.pololu.com/product/1996
Pololu Universal Aluminum Mounting Hub for 3mm Shaft, M3 Holes (2-Pack)

https://www.servocity.com/gear-motor-input-board-a/
https://www.servocity.com/3mm-bore-couplers/


platine servocity
https://www.servocity.com/90-rpm-micro-gear-motor/
https://www.servocity.com/lightweight-set-screw-hub-3mm-bore/

For the 6 V micro metal gearmotors

mx1508 driver (pas de pwm!)
    Operating voltage range: DC 2-9.6 V
    Max output current: 1.5 A (Peak 2 A)
    Max working temperature: 15 °C

**driver**
- DRV8838 single-channel motor : DRV8838 motor driver can deliver a continuous 1.7 A (1.8 A peak) to a single brushed DC motor. operating voltage range from 0 V to 11 V
- DRV8833 dual motor
- DRV8835 dual motor driver carrier (or DRV8835 shield for Arduino)


alim 12V : pwm 46 (/255)


## ALIMENTATION 220VAC - 12VDC 20A
- https://www.dalleled.fr/transformateur-led-meanwell-alimentation-240w-12v-20a?gclid=CjwKCAjwiLGGBhAqEiwAgq3q_mO-rSQj8Roha0D4Bh2XPj4KdXejcayZ3-G6M4nKbswfRH_6fajvWhoC7EAQAvD_BwE
- https://www.droneshop.com/alimentation-stabilisee-efuel-30a-skyrc
- https://www.droneshop.com/batteries-chargeurs/alimentation-secteur/alimentation-stabilisee-powerbase-138v-020a-t1266-t2m-p-4809.html
- https://www.amazon.fr/Alimentation-D%C3%A9coupage-Transformateur-Commutation-Convertisseur/dp/B08QDDY412/ref=sr_1_11?dchild=1&keywords=alimentation+12v+20a&qid=1624009364&sr=8-11
- https://fr.farnell.com/oyn-x/psu20-b/alimentation-ac-dc-12v-20a/dp/3532584?gclid=CjwKCAjwiLGGBhAqEiwAgq3q_qKyrkykMlPh1nvmLDFBoSjEme7SdzFa8t1jbCGH_lsDdwDOmJxW5hoCID4QAvD_BwE&mckv=spqJjijdA_dc|pcrid|459805414998|plid||kword||match||slid||product|3532584|pgrid|113368697211|ptaid|pla-338995327639|&CMP=KNC-GFR-SHOPPING-SMEC-01-Mar-21_Desk-Lo&gross_price=true
-Alimentation à découpage rack 19" Mean Well NMS-240-12 20 A 240 W 12 V/DC Nbr. de sorties: 1 x 1 pc(s) : https://www.conrad.fr/p/alimentation-a-decoupage-rack-19-mean-well-nms-240-12-20-a-240-w-12-vdc-nbr-de-sorties-1-x-1-pcs-2246038
- Alimentation 12V 20A 240W ventilée : https://3delectroshop.fr/alimentations/172-alimentation-a-decoupage-12v-20a.html
- Alimentation à découpage intégrée 240W, 1 sortie à 12V c.c. 20A : https://fr.rs-online.com/web/p/alimentations-a-decoupage/1618225/
- Weishuo 12V 20A DC 18 Output CCTV Box d'alimentation Alimentation Distribuée 240W pour Le Système de Sécurité CCTV DVR et Caméras  https://www.amazon.fr/HAILI-dalimentation-Alimentation-Distribu%C3%A9e-S%C3%A9curit%C3%A9/dp/B07SV35WTZ/ref=sr_1_8?dchild=1&keywords=alimentation+12v+20a&qid=1624009364&sr=8-8



### STRIP LED


https://github.com/FastLED/FastLED/wiki/Multiple-Controller-Examples


https://www.tweaking4all.com/hardware/arduino/arduino-ws2812-led/

    AdaFruit NeoPixel
    FastLED (FastSPI_LED)


There are 2 major types of LED strips that support multiple colors: Analog strips and Digital strips.
WS2801 has 4 pins, where as the WS2811/WS2812 only has 3 pins.

It’s important to pay attention to the arrow, if you use your strip in the wrong “direction”, it will not work.


Rule of thumb is : each RGB LED unit pulls about 60 mA (3x 20 mA, for Red, Green and Blue
(1 meter with 60 LEDs/meter =  60 * 60 mA = 3600 mA = 3.6 A max.)


Power connected to +5V or Vin?
 

In the drawing below, you’ll notice that I have used +5V pin for the powersupply.
This works well when you’re using a proper and well regulated powersupply, which I do.

For all correctness, and when using less regulated powersupplies, the Vin pin is recommended.

Pour un ruban de LED WS2812B, il faut compter 50mA par LED soit 2.5A pour 50 LED. 



Utilisation d’un ruban de LED WS2812B avec Arduino ( librairie FastLED.h. )
https://www.aranacorp.com/fr/utilisation-dun-ruban-de-led-ws2812b-avec-arduino/
ou Adafruit_NeoPixel.h
http://www.fablabredon.org/wordpress/2017/12/17/lumiere-sur-larduino-avec-de-la-couleur-et-des-led/

### HIGH POWER LED
- cree xm-l smd-led star rgbw 10W : https://www.lumitronix.com/fr_fr/cree-xm-l-smd-led-avec-platine-star-rgbw-68642.html
	- DC forward current 1A
	- Forward voltage @350mA red (2.25V - 2.6Vmax), green (3.3V - 3.9Vmax), blue/white (3.1V - 3.7Vmax)

Ressources:
- https://www.ledsupply.com/blog/constant-current-led-drivers-vs-constant-voltage-led-drivers/
- https://sound-au.com/lamps/thermal.html
- Note that the supply voltage must be at least 6.0V, and should be at least 2-3V higher than the forward voltage of the LED(s) to be driven.
- SparkFun According to Pete #40: LED Home Lighting and the FemtoBuck Driver : https://www.youtube.com/watch?v=yf9F3nqZH_Y
- DALI (Digital Addressable Lighting Interface) is a powerful protocol to control lighting. Through DALI you can dimmer your led lamps, ask them status, recall a predefined scenario and so on.
- https://create.arduino.cc/projecthub/NabiyevTR/simple-dali-controller-506e44
- It’s very essential to pay close attention to the PWM frequency as it’s usually in 100Hz-120Hz scale. High frequency PWM dimmers obviously eliminate visible frequency flicker on video cameras, so it’d be better to opt for a high frequency PWM version (10kHz to 25 kHz-50kHz) for extensive applications.


Adhésif thermique :
- Pâte thermique : 
	- https://www.amazon.fr/ARCTIC-MX-4-2019-Refroidisseur-Dissipateur/dp/B07L9BDY3T/ref=sr_1_5?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=pate+thermique&qid=1623959637&sr=8-5
- Adhésif thermique
- Fischer Elektronik Colle thermo-conductrice (2x2ml), WLK DK  : https://www.lumitronix.com/fr_fr/fischer-elektronik-colle-thermo-conductrice-2x2ml-wlk-dk-4-31986.html

Heatsink (dissipateur thermique, radiateur):
- Dissipateur thermique incurvé, 70mm : https://www.lumitronix.com/fr_fr/dissipateur-thermique-incurve-70mm-60023.html
- vissable : https://shop.saberproject.de/en/accessories/10/1-heat-sink-for-tri-cree-leds
- Dissipateur LED, Anodisé noir, S Series, Star LEDs, Aluminium, 4.27 °C/W, 25.4 mm, 69.8 mm : https://fr.farnell.com/ohmite/sa-led-125e/dissipateur-led-25-4mm-noir/dp/2097676

Driver :

- 300-3000mA Buck Regulator LED Driver for 1-100W High Power LED : https://store3.sure-electronics.com/ps-sp12153

- LDD-4 4 channel PWM : cutter.com ou coralus. 
	- https://rapidled.com/products/ldd-h-4-driver-board (Rapid LED Mean Well LDD-H-4)
	- https://fr.aliexpress.com/item/4001059869563.html
	- https://coralux.net/?wpsc-product=ldd-4-driver-board
	- https://www.cutter.com.au/product/coralux-ldd-5/
	
- SPARKFUN FemToBuck (1ch), PicoBuck (3ch) :  
https://www.gotronic.fr/art-driver-de-led-de-puissance-com-13716-24815.htm
https://learn.sparkfun.com/tutorials/picobuck-hookup-guide-v12 et https://learn.sparkfun.com/tutorials/femtobuck-constant-current-led-driver-hookup-guide-v12. basé sur un AL8805 permet de contrôler une led de puissance sous 350 mA ou 750mA (resistance). https://www.youtube.com/watch?v=vKvNu4hQBnM


- CODEMERCS LED-Warrior04 Starterkit (I2C) https://shop.codemercs.com/en/led-warrior04.html (PRIX!!)

- Availability: Out Of Stock/. Colibrì - Driver for power led RGBW : https://store.open-electronics.org/colibri
- https://boundarycondition.home.blog/2019/02/28/building-a-high-power-rgb-led-driver/
- DAP-04 Convertisseur à 4 canaux DALI-PWM : https://www.reichelt.com/fr/fr/convertisseur-4-canaux-dali-pwm-dap-04-p157749.html?PROVID=2810&gclid=EAIaIQobChMI3tC1_8if8QIVDuztCh1t7QjjEAQYAyABEgK3ZvD_BwE
- DALI Master shield for Arduino UNO https://www.ebay.fr/itm/254211672779
- https://www.pcboard.ca/high-power-led-driver-board-kit

- 3W 5-35V LED Driver 700mA PWM Dimming DC to DC Step-down Constant Current : https://fr.aliexpress.com/item/32858293803.html
- MakersDriver 2up Pro : https://www.ledsupply.com/led-drivers/makersdriver-2up-pro
- DIYMORE : Full Color RGB LED Strip Driver Module Shield. https://github.com/MrKrabat/LED-Strip-Driver-Module 


- tindie.com 4 channel PWM high power led shield
- tindie.com High Power Led Shield LM3404
