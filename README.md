# Totem Tech

Technical parts of the Totem project (electronics, software)

## SETUP
***IN (mega diymore)** :
- 6 ana (5 pot, 1 distance) : A0, A1, A2, A3, A4, A5
- 1 encoder :  D2 (CLK), D3 (DT)
- 6 digit (push buttons) : D4, D5, D6, D7, D8, D9

**OUT 1 (dmx)**
- rgbw LED COB 20W * 4

**OUT 2 (nano)**
- Power: 5V / 10A
- 3 digital LED strip : D2, D3, D4 (data pins for the 3 stips) and GND

**OUT 3 (nano)**
- Power: 12V / 2A
- the "Vin" pin is not connected to avoid power the motor with the Arduino
- micro-gear polulu motor 6v : D3 (speed), D12 (direction)

## ARDUINO CODE
- Dependancies : RotaryEncoder (Matthias Hertel), ezButton (ArduinoGetStarted), FastLED (fastled.io).
- For this Arduino Nano, choose Processor "ATmega328P old bootloader"

## PURE DATA CODE
- Pd version > 5.0 (for fudiparse, fudiformat)
- Dependancies : comport
